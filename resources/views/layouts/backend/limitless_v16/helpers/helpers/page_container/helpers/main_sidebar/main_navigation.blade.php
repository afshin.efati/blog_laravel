<div class="sidebar-category sidebar-category-visible">
    <div class="category-content no-padding">
        <ul class="navigation navigation-main navigation-accordion">

            <!-- Main -->
            <li class="navigation-header"><span>فهرست اصلی:</span> <i class="icon-menu" title="فهرست اصلی"></i></li>
            <li  class="active" ><a href=""><i class="icon-home4"></i> <span>داشبورد</span></a></li>
            <li>
                <a href="#"><i class="icon-folder-open"></i> <span>فرم های کارشناس</span></a>
                <ul>
                    <li ><a href=""><i class="fa fa-users"></i> <span>تولید سریال برگه ها</span></a></li>
                    <li ><a href=""><i class="fas fa-receipt"></i> <span>ثبت فیش پرداختی</span></a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-folder-open"></i> <span>فرم های تولید کننده</span></a>
                <ul>
                    <li ><a href=""><i class="fa fa-users"></i> <span>محصولات</span></a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-folder-open"></i> <span>حراجی</span></a>
                <ul>
                    <li ><a href=""><i class="fa fa-users"></i> <span>حراجی</span></a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-folder-open"></i> <span> مدیریت سیستم</span></a>
                <ul>
                    <li ><a href=""><i class="fa fa-users"></i> <span>مدیریت کاربران</span></a></li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="icon-folder-open"></i> <span>مدیریت افزونه ها</span></a>
               
            </li>
        </ul>
    </div>
</div>
