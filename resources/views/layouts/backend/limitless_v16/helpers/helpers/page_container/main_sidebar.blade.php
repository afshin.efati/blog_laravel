<div class="sidebar sidebar-main sidebar-fixed">
    <div class="sidebar-content">

        <!-- User menu -->
        {{--@include('layouts.backend.limitless_v16.helpers.helpers.page_container.helpers.main_sidebar.user_menu')--}}
        <!-- /user menu -->


        <!-- Main navigation -->
        @include('layouts.backend.limitless_v16.helpers.helpers.page_container.helpers.main_sidebar.main_navigation')
        <!-- /main navigation -->

    </div>
</div>