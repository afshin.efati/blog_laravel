<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        @include('layouts.backend.limitless_v16.helpers.helpers.page_container.main_sidebar')
        <!-- /main sidebar -->


        <!-- Main content -->
        @include('layouts.backend.limitless_v16.helpers.helpers.page_container.main_content')
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>