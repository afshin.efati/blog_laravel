<style>
    .dropdown-submenu {
        position: relative;
    }

    .dropdown-submenu a::after {
        transform: rotate(-90deg);
        position: absolute;
        right: 3px;
        top: 40%;
    }

    .dropdown-submenu:hover .dropdown-menu, .dropdown-submenu:focus .dropdown-menu {
        display: flex;
        flex-direction: column;
        position: absolute !important;
        margin-top: -30px;
        left: 100%;
    }

    @media (max-width: 992px) {
        .dropdown-menu {
            width: 50%;
        }

        .dropdown-menu .dropdown-submenu {
            width: auto;
        }
    }

   /* li.nav-item a {
        color: #ddd7ff !important;
    }*/
    .color_nav_item{
        color: #ddd7ff !important;
    }


    li.nav-item.active a {
        color: #ede6ff !important;
        font-weight: bolder;
        font-size: 16px;

    li.nav-item.active {
        background-color: #eeeeee2b !important;
    }
    }

    #btn_login_register_id:hover {
        color: #fff !important;
        border-color: #aea0ff !important;
    }

    #btn_login_register_id {
        background-color: rgba(150, 136, 226, 0.77);
        border: 1px solid #8579c5;
        color: #eee !important;
    }

    #xs_login_logout_btn_area {
        display: none;
    }

    @media screen and (max-width: 768px) {
        .navbar {
            padding: 0px 5px 0px 0px;
        }

        .navbar-brand {
            margin-left: 5px;
        }

        #xs_login_logout_btn_area {
            display: flex;
            width: 200px;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: justify;
            -ms-flex-pack: justify;
            justify-content: space-between;
            padding: 5px;
        }
    }
</style>
<div class="container-fluid bg-light">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="background-color: rgba(72, 51, 183,0.95) !important;">
            <a class="navbar-brand" href="{{route('frontend.index')}}">BlitChi.ir</a>
            <div id="xs_login_logout_btn_area">
                @if(auth()->check())
                    <a href="{{route('auth.logout')}}">
                        <button class="nav-link btn btn-sm color_nav_item">
                            <i class="fas fa-sign-out-alt fa-flip-horizontal"></i>
                            <span>خروج</span>
                        </button>
                    </a>
                @else
                    <a href="{{route('auth.login.index')}}">
                        <button class=" nav-link btn btn-sm color_nav_item" id="btn_login_register_id">
                            <i class="fas fa-sign-in-alt fa-flip-horizontal"></i>
                            <span>ورود / ثبت نام</span>
                        </button>
                    </a>
                @endif
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#master_navbar_menu" aria-controls="master_navbar_menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse ltr" id="slave_navbar_menu">
                <ul class="navbar-nav no-padding no-margin rtl">
                    <li class="nav-item @if(\Route::currentRouteName() == 'auth.login.index') active @endif">
                        @if(auth()->check())
                            <a class="nav-link btn btn-sm color_nav_item" href="{{route('auth.logout')}}">
                                <i class="fas fa-sign-out-alt fa-flip-horizontal"></i>
                                <span>خروج</span>
                            </a>
                        @else
                            <a class="nav-link btn btn-sm color_nav_item" href="{{route('auth.login.index')}}" id="btn_login_register_id">
                                <i class="fas fa-sign-in-alt fa-flip-horizontal"></i>
                                <span>ورود / ثبت نام</span>
                            </a>
                        @endif
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>

