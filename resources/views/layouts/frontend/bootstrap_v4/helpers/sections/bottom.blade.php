<div class="container-fluid bottom">
    <div class="">
        <div class="row">
            <div class="col-md-8 col-sm-12" style="min-height: 50px;">
                <h6 style="line-height: 50px;">
                    <span>با خدمات ۲۴ ساعته مرکز رزرواسیون رویدادهای هنری (بلیتچی)</span>
                    <span style="color:#7b7b7b">، در ۷ روز هفته همراه ما باشید:</span>
                </h6>
            </div>
            <div class="col-md-4 col-sm-12 text-left ltr" style="background: #E8E8E8; min-height: 50px; line-height: 50px; font-weight: bold;">
                <div class="float-right" style=" margin-top: 5px; margin-right: 10px;">
                    <i class="fa fa-2x fa-phone-volume"></i>
                </div>
                <div class="float-right">
                    <span style="color: #84c50a;">۰۲۱</span>
                    <span> </span>
                    <span style="color: #555">۴۴۸۲۰۹۳۱</span>
                </div>
                <div class="float-right" style="margin: 0 15px;">
                    <span>|</span>
                </div>
                <div class="float-right">
                    <span style="color: #84c50a;">۰۹۰۱</span>
                    <span> </span>
                    <span style="color: #555">۱۳۳۸۵۱۰</span>
                </div>
            </div>
            <div class="clearfixed"></div>
        </div>
        <div class="clearfixed"></div>
    </div>
    <div class="hr hr-2"></div>
    <div class="space-20"></div>
    <div class="">
        <div class="row">
            <div class="col-md-3 col-sm-12" style=" min-height: 120px;">
                <h6 class="" style="font-weight: bolder">
                    <span>بلیتچی:</span>
                </h6>
                <ul class="">
                    <li class="li_menu_bottom_items">
                        <a class="li_menu_bottom_item_a" href="{{route('frontend.about')}}">
                            <span>درباره ما</span>
                        </a>
                    </li>
                    <li class="li_menu_bottom_items">
                        <a class="li_menu_bottom_item_a" href="{{route('frontend.contact')}}">
                            <span>ارتباط با ما</span>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="col-md-3 col-sm-12" style=" min-height: 120px;">
                <h6 class="" style="font-weight: bolder">
                    <span>اطلاعات تماس</span>
                </h6>
                <ul class="no-padding no-margin" style="list-style: none; text-align: justify;">
                    <li>
                        <span style="font-weight: bolder; font-size: 14px;">آدرس:</span>
                        <span style="color: #444; font-size: 14px; "> تهران، انتهای اشرفی اصفهانی، شهرک نفت، خیابان ۱۶، پلاک ۱ ، واحد ۳۴</span>
                    </li>
                    <li>
                        <span style="font-weight: bolder; font-size: 14px;">تلفن پشتیبانی:</span>
                        <a href="tel:02144820931" style="float:left;">
                            <span>۴۴۸۲۰۹۳۱</span>
                            <span> - </span>
                            <span style="color:green">۰۲۱</span>
                        </a>
                    </li>
                    <li>
                        <span style="font-weight: bolder; font-size: 14px;">ایمیل:</span>
                        <a style="float:left" href="mailto:support@blitchi.ir">
                            <span>support@blitchi.ir</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12" style=" min-height: 120px;">
                {{--<img style="height: 125px;width: 125px;" src="{{asset('assets/img/licenses/enamad_logo.png')}}">--}}
                {{--<img style="height: 125px;width: 125px;" src="{{asset('assets/img/licenses/samandehi_logo.png')}}">--}}
            </div>
            <div class="clearfixed"></div>
        </div>
    </div>
    <div class="hr hr-8"></div>
    <div class=" hidden-sm" style=" min-height: 30px;">
        <div class="row">
            <div class="col-12 text-center smaller-80">
                <div class="space-6"></div>
                <div>
                    <a href="" rel="follow">بلیط نمایشگاه</a>
                    <span> | </span>
                    <a href="" rel="follow">بلیط سینما</a>
                    <span> | </span>
                    <a href="" rel="follow">خرید بلیط تئاتر</a>
                    <span> | </span>
                    <a href="" rel="follow">خرید بلیط کنسرت</a>
                    <span> | </span>
                    <a href="" rel="follow"> فروش بلیط آنلاین</a>
                    <span> | </span>
                    <a href="" rel="follow">خرید بلیط با انتخاب صندلی</a>
                    <span> | </span>
                    <a href="" rel="follow">فروش بلیط کنسرت</a>
                </div>
                <div class="space-8"></div>
            </div>
            <div class="clearfixed"></div>
        </div>
    </div>
    <div class="clearfixed"></div>
</div>