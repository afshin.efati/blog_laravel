<div class="container-fluid footer">
    <div class="">
        <div class="row">
            <div class="col-8">
                <h6 style="font-size: 14px;line-height: 40px;margin: 0;">
                    <span> Ⓒ </span>
                    <span>کلیه حقوق این سایت محفوظ و متعلق به بلیتچی می باشد.</span>
                </h6>
                <div class="clearfixed"></div>
            </div>
            <div class="col-4 ltr text-left" style="font-size: 25px;">
                <a href="https://www.aparat.com/blitchi" style="margin: 0 5px;"><i class="fas fa-film"></i></a>
                <a href="https://www.instagram.com/blitchi.ir" style="margin: 0 5px;"><i class="fab fa-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UCZ-DkwK07pbRxMUIesJzYNA" style="margin: 0 5px;"><i class="fab fa-youtube"></i></a>
                <a href="https://t.me/blitchi" style="margin: 0 5px;"><i class="fab fa-telegram"></i></a>
                <div class="clearfixed"></div>
            </div>
            <div class="clearfixed"></div>
        </div>
    </div>
    <div class="clearfixed"></div>
</div>