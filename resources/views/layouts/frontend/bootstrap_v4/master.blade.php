<!doctype html>
<html lang="{{ app()->getLocale() }}" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('page_title','BlitChi.ir - بلیتچی سامانه رزرو آنلاین بلیت کنسرت ')</title>

    <!-- Bootstrap CSS -->
@include('layouts.frontend.bootstrap_v4.helpers.css.core_css')
<!-- Optional CSS -->
@yield('plugin_css')
@yield('inline_css')

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
@include('layouts.frontend.bootstrap_v4.helpers.js.core_js')

<!-- Optional JavaScript -->
    @yield('plugin_js')
    @yield('inline_js')
</head>
<body>
<div class="main @yield('main_class')">
    @include('layouts.frontend.bootstrap_v4.helpers.sections.top_banner')
    @include('layouts.frontend.bootstrap_v4.helpers.sections.main_menu')
    @yield('content')
    @include('layouts.frontend.bootstrap_v4.helpers.sections.bottom')
    @include('layouts.frontend.bootstrap_v4.helpers.sections.footer')
</div>

<!-- Optional JavaScript -->
@yield('footer_plugin_js')
@yield('footer_inline_js')

@if(session()->has('pm_message') || session()->has('pm_title'))
    <script>
        $(document).ready(function () {
            var type = '{{ @session()->pull('pm_type') }}';
            if (type !== 'error' && type !== 'success' && type !== 'info')
            {
                type = 'info';
            }
            var opts = {
                addclass: "stack-bottomleft",
                title: '{!! @session()->pull('pm_title') !!}',
                text: '{!! @session()->pull('pm_message') !!}',
                type : type
            };
            new PNotify(opts);
        });
    </script>
@endif

</body>
</html>
