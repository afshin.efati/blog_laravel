@extends('layouts.backend.limitless_v16.master')
@section('breadcrumb')
    <li class="active">مدیریت دسته بندی</li>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="tabbable">
                        <ul class="nav nav-tabs nav-tabs-bottom" id="manage_basic_data">
                            <li id="tab_manage_basic_data_tab" class="active">
                                <a href="#manage_tab_basic_data" data-toggle="tab" id="tab_link_manage_basic_data_tab">مدیریت</a>
                            </li>
                            <li id="tab_add_tab"><a href="#add_basic_data_tab" data-toggle="tab">افزودن</a></li>
                            <li id="div_edit_basic_data" class="hide">
                                <button class="close closeTab cancel_edit_basic_data" type="button">×</button>
                                <span class="divider">|</span>
                                <a href="#edit_basic_data_tab" style="margin-left: 10px" data-toggle="tab"
                                   class="edit_basic_data" id="">ویرایش</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="basic_data_tab_content">
                            <div class="tab-pane active" id="manage_tab_basic_data">
                                <table id="mytable" class="table">

                                </table>
                            </div>
                            <div class="tab-pane " id="add_basic_data_tab">
                                <form id="frm_add_basic_data">
                                    <div class="space-10"></div>
                                    <div class="form-group fg_company_type">
                                        <label class="col-sm-3 control-label label_post" for="title">
                                            <span class="more_info"></span>
                                            <span class="label_title">عنوان</span>
                                        </label>
                                        <div class="col-sm-5">
                                            <input name="title" class="form-control" id="basic_data_value_add_title"
                                                   tab="2">
                                        </div>
                                        <div class="col-sm-4 messages"></div>
                                        <div class="clearfixed"></div>
                                    </div>
                                    <div class="row text-left">
                                        <button type="button" class="btn btn-labeled cancel_edit_basic_data_value"><b><i
                                                    class="fa fa-times"></i></b>لغو
                                        </button>
                                        <button type="submit" class="btn bg-teal-400 btn-labeled add_product"><b><i
                                                    class="fa fa-save"></i></b>ذخیره
                                        </button>
                                    </div>
                                </form>


                            </div>
                            <div class="tab-pane " id="edit_basic_data_tab">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer_plugin_js')
    @include('backend.categories.helpers.script')

@endsection
