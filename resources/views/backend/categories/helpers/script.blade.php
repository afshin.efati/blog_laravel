<script>
    $(document).ready(function () {
        window['basic_data_grid_columns'] = [
            {
                "data": "id", 'title': 'ردیف',
                searchable: false,
                sortable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                data: 'title',
                name: 'title',
                title: 'عنوان ',
            },
            {
                data: 'title', name: 'title', 'title': 'عنوان',
                mRender: function (data, type, full) {
                    return '<button class="btn btn-primary edit_product" data-id="' + full.id + '">' +
                        'edit' +
                        '' +
                        '</button>'
                }
            },
        ];
        var data = {
            id: 3
        }
        var getBasicdataRoute = '{{ route('backend.category.datatable') }}';
        dataTablesGrid('#mytable', 'mytable', getBasicdataRoute, basic_data_grid_columns, data);
    });

    $(document).off('click', '.edit_product');
    $(document).on('click', '.edit_product', function () {
        var itme_id = $(this).data('id');
        edit_product(itme_id);
    });

  
</script>
