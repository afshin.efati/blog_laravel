<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix' => 'backend', 'namespace' => 'Backend'],function () {
    Route::group(['prefix' => 'categories', 'namespace' => 'Category'], function () {
        Route::get('/', ['as' => 'backend.category.index', 'uses' => 'CategoryController@index']);
        Route::post('/datatable', ['as' => 'backend.category.datatable', 'uses' => 'CategoryController@datatable']);

    });


});


