function set_fixed_dropdown_menu(e)
{
    var position = $(e).offset() ;
    var scrollTop  = $(document).scrollTop() ;
    var drop_height = $(e).find('.dropdown-menu-right').height() +16;
    if(($(window).height() - position.top)>drop_height)
    {
        $(e).find('.dropdown-menu').css({'position':'fixed','top':position.top-scrollTop+16,'left':position.left+22,'height':'fit-content'});
        window.addEventListener("scroll", function (event) {
            var scroll = this.scrollY;
            $(e).find('.dropdown-menu').css('top',position.top-scroll+16)
        });
    }
    else
    {
        $(e).find('.dropdown-menu').css({'position':'fixed','top':position.top-scrollTop+16-drop_height,'left':position.left+22,'height':'fit-content'});
        window.addEventListener("scroll", function (event) {
            var scroll = this.scrollY;
            $(e).find('.dropdown-menu').css('top',position.top-scroll+16-drop_height)
        });
    }
}


