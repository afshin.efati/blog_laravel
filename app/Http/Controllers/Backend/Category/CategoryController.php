<?php

namespace App\Http\Controllers\Backend\Category;

use App\Category;
use DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index()
    {
       return view('backend.categories.index');
    }
    public function datatable(Request $request){
        $cat=Category::all();
        return DataTables::of($cat)->make(true);

    }
}
